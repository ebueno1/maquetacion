import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent} from './../modal/modal.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(public matdialog:MatDialog) { }

  openContact (){
    const models = this.matdialog.open(ModalComponent)
    models.afterClosed().subscribe(x => {
      // console.log(`El resultado es :${x}`);
      
    })
  }

  ngOnInit(): void {
  }

}
